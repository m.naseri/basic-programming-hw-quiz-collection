// MohammadMahdiNaseri_9423115
// HM1_Ex2
// Convert Binary to Decimal

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	int x{}, xprime{}; // x: the number (binary); xprime: the number (decimal);
	int x0{}, x1{}, x2{}, x3{}, x4{}; // digits of the number (x = x4x3x2x1x0) 
	int temp; // temp: just temporary;
	cout << "Convert Binary to Decimal" << "\n\n"
		<< "Please enter a number (at most 5 digits) in binary base: ";
	cin >> x;
	if (x < 1E5)
		/* digits count (We assume that we can't use "while" & Also
		we consider for example "00011" as a correct input) */
	{
		x0 = x % 10;
		temp = x / 10;
		x1 = temp % 10;
		temp /= 10;
		x2 = temp % 10;
		temp /= 10;
		x3 = temp % 10;
		temp /= 10;
		x4 = temp % 10;
		if ((x0 == 0 || x0 == 1) && (x1 == 0 || x1 == 1) && (x2 == 0 || x2 == 1)
			&& (x3 == 0 || x3 == 1) && (x4 == 0 || x4 == 1)) // binary base check
		{
			xprime = (x0)+(x1 * 2) + (x2 * (4)) + (x3 * (8)) + (x4 * (16));
			cout << x << " =====> " << xprime << "\n";
		}
		else
			cout << "Error: You didn't enter a binary base number." << "\n";
	}
	else
		cout << "Error: Your number digits are more than 5." << "\n";
	return 0;
}