// MohammadMahdiNaseri_9423115
// HM1_Ex5

#include <iostream>

using std::cout;
using std::endl;

int main()
{
	int x{ 20 };
	unsigned int y{ 30 };
	int yprime = static_cast<int>(y);
	cout << x - yprime << endl;
	//or:
	cout << static_cast<int>(x - y) << endl;
	return 0;
}