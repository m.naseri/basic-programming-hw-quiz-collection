// MohammadMahdiNaseri_9423115
// HM1_Ex3

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	double dx1{}, dx2{}, dx3{}; // the numbers
	int ix1{}, ix2{}, ix3{}; // casted numbers
	double sum{}, average{}, product{}; // of the numbers
	cout << "Please enter 3 numbers: ";
	cin >> dx1 >> dx2 >> dx3;
	ix1 = static_cast<int>(dx1);
	ix2 = static_cast<int>(dx2);
	ix3 = static_cast<int>(dx3);
	if (dx1 - ix1 == 0) // recognize the types of numbers
	{
		if (dx2 - ix2 == 0)
		{
			if (dx3 - ix3 == 0)
			{
				// x1: int, x2: int, x3: int;
				sum = ix1 + ix2 + ix3;
				product = ix1 * ix2 * ix3;
				cout << "Type of variables: int, int, int" << endl;
			}
			else
			{
				// x1: int, x2: int, x3: double;
				sum = ix1 + ix2 + dx3;
				product = ix1 * ix2 * dx3;
				cout << "Type of variables: int, int, double" << endl;
			}
		}
		else 
		{
			if (dx3 - ix3 == 0)
			{
				// x1: int, x2: double, x3: int;
				sum = ix1 + dx2 + ix3;
				product = ix1 * dx2 * ix3;
				cout << "Type of variables: int, double, int" << endl;
			}
			else
			{
				// x1: int, x2: double, x3: double;
				sum = ix1 + dx2 + dx3;
				product = ix1 * dx2 * dx3;
				cout << "Type of variables: int, double, double" << endl;
			}
		}
	}
	else
	{
		if (dx2 - ix2 == 0)
		{
			if (dx3 - ix3 == 0)
			{
				// x1: double, x2: int, x3: int;
				sum = dx1 + ix2 + ix3;
				product = dx1 * ix2 * ix3;
				cout << "Type of variables: double, int, int" << endl;
			}
			else
			{
				// x1: double, x2: int, x3: double;
				sum = dx1 + ix2 + dx3;
				product = dx1 * ix2 * dx3;
				cout << "Type of variables: double, int, double" << endl;
			}
		}
		else
		{
			if (dx3 - ix3 == 0)
			{
				// x1: double, x2: double, x3: int;
				sum = dx1 + dx2 + ix3;
				product = dx1 * dx2 * ix3;			
				cout << "Type of variables: double, double, int" << endl;
			}
			else
			{
				// x1: double, x2: double, x3: double;
				sum = dx1 + dx2 + dx3;
				product = dx1 * dx2 * dx3;
				cout << "Type of variables: double, double, double" << endl;
			}
		}
	}
	average = sum / 3;
	cout << "Sum is: " << sum << endl
		<< "Product is: " << product << endl
		<< "Average is: " << average << endl;
	return 0;
}