// MohammadMahdiNaseri_9423115
// HW3_Ex1

#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	cout << "N: ";
	int N{};
	cin >> N;

	cout << "The Pattern: " << endl;
	for (int i = 1; i <= N; i++)
	{
		for (int j = i; j <= 2 * i - 1; j++)
		{
			cout << j << " ";
		}
		for (int j = 2 * i - 2; j >= i; j--)
		{
			cout << j << " ";
		}
		cout << endl;
	}

	return 0;
}