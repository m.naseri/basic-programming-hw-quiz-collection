// MohammadMahdiNaseri_9423115
// HW3_Ex3

#include<iostream>
#include<vector>
#include<cmath>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	cout << "*** Prime Numbers ***" << endl
		<< "Max: ";
	int N{};
	cin >> N;
	if (N <= 0)
	{
		cout << "Error! Max should be more than 0..." << endl;
		return 0;
	}

	std::vector<int> Number;
	Number.reserve(N - 1); // 2, 3, 4, ..., N

	for (int i{ 2 }; i <= N; i++) // the vector values
		Number.push_back(i);

	for (int i{ 2 }; i <= sqrt(N); i++) // prime numbers detection
		if (Number[i - 2] != 0)
			for (int j{ i - 1 }; j < N - 1; j++)  // * j {i - 1}: (i + 1) - 2; ** Number[j] = j - 2;
				if (Number[j] % i == 0)
					Number[j] = 0;

	for (int i : Number) // print
		if (i != 0)
			cout << i <<  "\t";
	cout << endl;

	return 0;
}