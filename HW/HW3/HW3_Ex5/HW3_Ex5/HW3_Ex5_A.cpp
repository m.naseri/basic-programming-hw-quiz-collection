// MohammadMahdiNaseri_9423115
// HW3_Ex5

#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<iomanip>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	srand(time(NULL));
	int table[4][4]{};

	for (size_t i = 0; i < 4; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			if (!(i == 3 && j == 3))
			{
				int temp{};
				bool flag{ false }; // Non-repetitive flag
				while (!flag) // Non-repetitive number creater
				{
					temp = rand() % 15 + 1;
					flag = true;
					for (size_t iprime{ 0 }; iprime < 4; iprime++)
					{
						for (size_t jprime{ 0 }; jprime < 4; jprime++)
						{
							if (table[iprime][jprime] == temp)
							{
								flag = false;
								break;
							}
						}
					}
				}
				table[i][j] = temp;
			}
		}
	}

	cout << "The Array: " << endl;
	for (size_t i = 0; i < 4; i++) // print
	{
		for (size_t j = 0; j < 4; j++)
			cout << std::setw(2) << table[i][j] << '\t';
		cout << endl;
	}
	return 0;
}