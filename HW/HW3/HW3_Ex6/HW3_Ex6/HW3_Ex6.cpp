// MohammadMahdiNaseri_9423115
// HW3_Ex6

#include<iostream>
#include<vector>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	/* Cin */
	int X{}, Y{}; // early and secondary base
	cout << "Early Base (>1): ";
	cin >> X;
	cout << "Secondary Base (>1): ";
	cin >> Y;
	cout << "The number in Early Base: " << endl
		<< "(for example: 10 20 3 21 3 1 0 23 (with one space between each part) "
		<< "then Enter -1)" << endl;
	std::vector<int> Number1; // the number in early base
	Number1.reserve(100);
	int temp{};
	while (temp != -1)
	{
		cin >> temp;
		Number1.push_back(temp);
	}
	Number1.pop_back(); // delete -1
	
	/* Convert to decimal */
	unsigned int N{ Number1.size() };
	int TheNumber{}; // the number in decimal
	for (size_t i = 0; i < N; i++)
	{
		if (Number1[i] >= X)
		{
			cout << "Error! the parts should be less than early base value... :( ";
			return 0;
		}
		else
		TheNumber += Number1[i] * static_cast<int>(pow(X, (N - 1) - i));
	}
	
	/* Convert to Y Base */
	temp = TheNumber;
	std::vector<int> Number2; // the number in secondary base
	Number2.reserve(100);
	while (temp != 0)
	{
		Number2.push_back(temp % Y);
		temp /= Y;
	}

	/* Print */
	cout << "The number in Secondary Base: " << endl;
	N = Number2.size();
	for (int i{ static_cast<int>(N - 1) }; i >= 0; i--)
	{
		cout << Number2[i] << ' ';
	}
	cout << endl;

	return 0;
}