// MohammadMahdiNaseri_9423115
// HW3_E2

#include<iostream>
#include<vector>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	cout << "*** Bubble Sort ***" << endl;
	cout << "Please ENTER the numbers for sorting" << endl
		<< "* for ending the input operation please ENTER \"0\"" << endl;

	/* Cin */
	std::vector<int> Num;
	Num.reserve(30);
	int temp{ 1 }, i{}; // temporary & counter
	for (i = 1; temp != 0; i++)
	{
		cout << "Number " << i << ": ";
		cin >> temp;
		Num.push_back(temp);
	}
	Num.pop_back(); // delete "0"
	cout << "You have entered " << i - 2 << " numbers" << endl;

	/* Sort */
	bool SortFlag{ false };
	int NumSize = Num.size();
	while (!SortFlag)
	{
		SortFlag = true;
		for (int i{}; i <= NumSize - 2; i++)
		{
			if (Num[i] > Num[i + 1])
			{
				SortFlag = false;
				temp = Num[i + 1];
				Num[i + 1] = Num[i];
				Num[i] = temp;
			}
		}
	}
	
	/* Print */
	cout << "Sorted numbers: ";
	for (int i : Num)
		cout << i << ' ';
	cout << endl;
	
	return 0;
}