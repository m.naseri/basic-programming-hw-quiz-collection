// MohammadMahdiNaseri_9423115
// HW3_Ex4

#include<iostream>
#include<stdlib.h>
#include<time.h>
#define _USE_MATH_DEFINES
#include<math.h>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	srand(time(NULL));
	const int R{ 50 };
	double pi{};
	int CDot{}; // Dots in the circle
	int SDot{}; // All dots

	while (abs(M_PI - pi) >= 1E-2)
	{
		int x{ rand() % (2 * R) + 1 };
		int	y{ rand() % (2 * R) + 1 };
		SDot++;
		if (pow((x - R), 2) + pow((y - R), 2) <= pow(R, 2))
			CDot++;
		pi = 4 * static_cast<double>(CDot) / static_cast<double>(SDot);
		cout << pi << endl;
	}

	return 0;
}