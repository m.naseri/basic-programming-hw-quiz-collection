// MohammadMahdiNaseri_9423115
// HW3_Ex7

#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int i{}; // global counter

void APrint();
void BPrint();

int main()
{
	APrint();
	i--; // 101 => 100
	BPrint();
	cout << endl;

	return 0;
}

void APrint()
{
	if (i <= 100)
	{
		cout << i << ' ';
		i++;
		APrint();
	}
}

void BPrint()
{
	if (i >= 0)
	{
		cout << i << ' ';
		i--;
		BPrint();
	}
}