// MohammadMahdiNaseri_9423115
// HW2_Ex3

#include<iostream>
#include<vector>
#include<iomanip>

using std::cout;
using std::endl;
using std::cin;

int main()
{
	cout << "* Prime or NotPrime *" << endl;
	while (true)
	{
		cout << "N (>1): ";
		int num{};
		cin >> num;
		if (num > 1)
		{
			std::vector<int> divisor; // for num divisors
			divisor.reserve(num);
			int temp{ num }; // just temporary
			int DigitCounter{}; // number digits counter (for setw process)

			/* Difgits counting */
			for (DigitCounter = 0; temp > 0; DigitCounter++)
				temp /= 10;

			/* Divisors finding */
			for (int i{ 1 }; i <= num; i++)
				if (num % i == 0)
					divisor.push_back(i);
			int DivisorCounter{ static_cast<int>(divisor.size()) }; // improve speed

			/* Print */
			if (DivisorCounter == 2)
				cout << num << " is prime." << endl;
			else
			{
				cout << num << " is not prime:" << endl;
				for (int i{}; i <= DivisorCounter - 1; i++)
					if (divisor[i] <= divisor[DivisorCounter - 1 - i]) // the '=' is for numbers which are like a^2
						cout << num << " = " << std::setw(DigitCounter) << divisor[i] << " x "
						<< std::setw(DigitCounter) << divisor[DivisorCounter - 1 - i] << "\n";
			}
		}
		else
			cout << "The number should be more than 1..." << endl;
	}
	return 0;
}