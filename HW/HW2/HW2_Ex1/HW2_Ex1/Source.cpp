// MohammadMahdiNaseri_9423115
// HW2_Ex1

#include<iostream>
#include<vector>

using std::cout;
using std::endl;
using std::cin;

int main()
{
	cout << "* Inverse The Numbers *" << endl;
	std::vector<short> Num; // inputted number
	Num.reserve(100);
	cout << "The Number (Example: 123456* (then press ENTER))): ";
	char temp{}; // temporary
	int OChecker{ false }; // 0 Checker!
	while (temp != '*') // get the number
	{
		cin >> temp;
		if (temp != '0') // the first non 0 digit
			OChecker = true;
		if (OChecker)
			Num.push_back(static_cast<short>(temp - '0'));
	}
	Num.pop_back(); // remove the star
	const unsigned int N{ Num.size() }; // the vector lenght

	// we can just print without a new vector initialization:
	OChecker = false;
	cout << "Inverted Number: ";
	for (size_t i{}; i <= N - 1; i++) // inverse and print
	{
		if (Num[(N - 1) - i] != 0) // the first non 0 digit
			OChecker = true;
		if (OChecker)
			cout << Num[(N - 1) - i];
	}
	cout << endl;

	return 0;
}