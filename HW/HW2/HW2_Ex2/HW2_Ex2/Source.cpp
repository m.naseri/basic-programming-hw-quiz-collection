// MohammadMahdiNaseri_9423115
// HW2_Ex2

#include<iostream>
#include<vector>

using std::cout;
using std::endl;
using std::cin;

int main()
{
	int num{}; // the number
	cout << "The Number (>0): ";
	cin >> num;
	if (num > 0)
	{
		int temp{num}; // just temporary

		/* digit counter */
		int DigitCounter{};
		for (DigitCounter = 0; temp > 0; DigitCounter++)
			temp /= 10;

		/* digits separation */
		std::vector<short> NumPrime; // digits of the number
		NumPrime.reserve(50);
		temp = num;
		short digit{};
		while (temp > 0)
		{
			digit = temp % 10;
			NumPrime.push_back(digit);
			temp /= 10;
		}

		/* print the pattern */
		cout << "The Pattern: ";
		for (int i{ DigitCounter - 1 }; i >= 0; i--)
			for (int j{1}; j <= NumPrime[i]; j++)
				cout << NumPrime[i];
		cout << endl;
	}
	else
		cout << "The Number should be positive..." << endl;
	return 0;
}