// MohammadMahdiNaseri_9423115
// HW2_Ex3

#include<iostream>
#include<iomanip>
#include<cmath>

using std::cout;
using std::endl;
using std::cin;
using std::setw;
using std::setprecision;

int main()
{
	double x{ 1 }, f{}, fprime{}, eps{ 1 };
	cout << setw(3) << "i" << " " << setw(20) << "XOld" << " "
		<< setw(20) << "XNew" << " " << setw(20) << "f" << " "
		<< setw(20) << "fprime" << " " << setw(20) << "eps" << "\n";

	for (int i{1}; eps >= 1E-4; i++)
	{
		f = tan(x) - exp(x);
		fprime = 1/pow(cos(x), 2) - exp(x);
		eps = abs(f / (fprime * x));

		cout << setw(3) << i << " "
			<< setprecision(10) << setw(20) << x << " ";
		x = x - f / fprime;
		cout << setprecision(10) << setw(20) << x << " "
			<< setprecision(10) << setw(20) << f << " "
			<< setprecision(10) << setw(20) << fprime << " "
			<< setprecision(10) << setw(20) << eps << "\n";
	}
	return 0;
}