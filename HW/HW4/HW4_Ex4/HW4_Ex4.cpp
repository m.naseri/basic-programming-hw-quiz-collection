// MohammadMahdiNaseri_9423115
// HW4_Ex4

#include<iostream>
#include<iomanip>
#include<cmath>

using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::setw;

double f(const double& x);

int main()
{
	double x0{ -1 }, x1{ 0 };
	cout << "i" << ' ' << setw(15) << "x0" << ' '
		<< setw(15) << "f(x0)" << ' ' << setw(15) << "x1" << ' '
		<< setw(15) << "f(x1)" << setw(15) << "|x0 - x1|" << endl;
	for (size_t i{ 1 }; abs(x1 - x0) > .0; i++)
	{
		double temp{ x1 - f(x1) * ((x1 - x0) / (f(x1) - f(x0))) };
		cout << i << ' ' << setw(15) << x0 << ' '
			<< setw(15) << f(x0) << ' ' << setw(15) << x1 << ' '
			<< setw(15) << f(x1) << setw(15) << abs(x0 - x1) << endl;
		x0 = x1;
		x1 = temp;
	}
	cout << '\n' << "f(" << x0 << ")" << " = " << f(x0) << endl;
	return 0;
}

double f(const double& x)
{
	return x*x - sin(x) - .5;
}