// MohammadMahdiNaseri_9423115
// HW4_Ex2

#include<iostream>

using std::cout;
using std::cin;
using std::endl;

void PrintArray(int arr[], const int &N);

int main()
{
	const unsigned int N{ 10 }; // array length
	int arr[N]{};

	for (size_t i{}; i < N; i++)
		*(arr + i) = i;
	
	PrintArray(arr, 0);
	return 0;
}

void PrintArray(int arr[], const int &N)
{
	static size_t i{}; //counter
	if (N < 1 && i == 0)
		cout << "Error! The array length should be more than 0" << endl;
	else if (i < N)
	{
		cout << *(arr + (i++)) << ' ';
		PrintArray(arr, N);
	}
}