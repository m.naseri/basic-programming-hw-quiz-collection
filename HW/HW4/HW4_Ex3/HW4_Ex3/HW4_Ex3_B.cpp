// MohammadMahdiNaseri_9423115
// HW4_Ex3_B

#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<iomanip>
#include<cmath>

using std::cout;
using std::cin;
using std::endl;

void RandomArr(int* parr[], const size_t& N, const size_t& M);
void PrintArr(int* parr[], const size_t& N, const size_t& M);
double AverageArr(int* parr[], const size_t& N, const size_t& M);
double DeviationArr(int* parr[], const size_t& N, const size_t& M);

int main()
{
	srand(time(NULL));
	const size_t N{ 10 }; // array size row (const)
	size_t M{ 10 }; // array size column (non const)
	int* parr[N]{};
	for (auto& i : parr)
		i = new int[M];

	RandomArr(parr, N, M);
	cout << "The Random Array: " << endl;
	PrintArr(parr, N, M);
	cout << "Average: " << AverageArr(parr, N, M) << endl;
	cout << "Deviation: " << DeviationArr(parr, N, M) << endl;

	for (auto& i : parr)
		delete[] i;

	return 0;
}

void RandomArr(int* parr[], const size_t& N, const size_t& M)
{
	for (size_t i{}; i < N; i++)
		for (size_t j{}; j < M; j++)
			*(*(parr + i) + j) = rand() % 1000;
}

void PrintArr(int* parr[], const size_t& N, const size_t& M)
{
	for (size_t i{}; i < N; i++)
	{
		for (size_t j{}; j < M; j++)
			cout << std::setw(3) << *(*(parr + i) + j) << "  ";
		cout << endl;
	}
}

double AverageArr(int* parr[], const size_t& N, const size_t& M)
{
	int sum{};
	for (size_t i{}; i < N; i++)
		for (size_t j{}; j < M; j++)
			sum += *(*(parr + i) + j);
	return sum / (N * M);
}

double DeviationArr(int* parr[], const size_t& N, const size_t& M)
{
	double average{ AverageArr(parr, N, M) };
	int SumPrime{};
	for (size_t i{}; i < N; i++)
		for (size_t j{}; j < M; j++)
			SumPrime += (*(*(parr + i) + j)) * (*(*(parr + i) + j));
	return sqrt(SumPrime / (N * M));
}