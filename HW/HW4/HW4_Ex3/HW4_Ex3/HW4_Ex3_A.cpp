// MohammadMahdiNaseri_9423115
// HW4_Ex3_A

#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<iomanip>
#include<cmath>

using std::cout;
using std::cin;
using std::endl;

void RandomArr(int* parr, const size_t& N, const size_t& M);
void PrintArr(int* parr, const size_t& N, const size_t& M);
double AverageArr(int* parr, const size_t& N, const size_t& M);
double DeviationArr(int* parr, const size_t& N, const size_t& M);

int main()
{
	srand(time(NULL));
	const size_t N{ 10 }; // array size row
	const size_t M{ 10 }; // array size column
	int arr[N][M]{};
	int* parr = &arr[0][0];

	RandomArr(parr, N, M);
	cout << "The Random Array: " << endl;
	PrintArr(parr, N, M);
	cout << "Average: " << AverageArr(parr, N, M) << endl;
	cout << "Deviation: " << DeviationArr(parr, N, M) << endl;
	return 0;
}

void RandomArr(int* parr, const size_t& N, const size_t& M)
{
	for (size_t i{}; i < N * M; i++)
		*(parr + i) = rand() % 1000;
}

void PrintArr(int* parr, const size_t& N, const size_t& M)
{
	for (size_t i{}; i < N; i++)
	{
		for (size_t j{}; j < M; j++)
			cout << std::setw(3) << *(parr + (i*M + j)) << "  ";
		cout << endl;
	}
}

double AverageArr(int* parr, const size_t& N, const size_t& M)
{
	int sum{};
	for (size_t i{}; i < N * M; i++)
		sum += *(parr + i);
	return sum / (N * M);
}

double DeviationArr(int* parr, const size_t& N, const size_t& M)
{
	double average{ AverageArr(parr, N, M) };
	int SumPrime{};
	for (size_t i{}; i < N * M; i++)
		 SumPrime += (*(parr + i) - average) * (*(parr + i) - average);
	return sqrt(SumPrime / (N * M));
}