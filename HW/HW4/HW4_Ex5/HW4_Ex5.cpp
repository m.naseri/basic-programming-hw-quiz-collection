// MohammadMahdiNaseri_9423115
// HW4_Ex5

#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<iomanip>
#include<vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

void RandomArr(vector< vector<int> >& arr, const size_t& N, const size_t& M);
void PrintArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M);
double AverageArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M);
double DeviationArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M);
int DeterminantArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M);

int main()
{
	srand(time(NULL));
	const size_t N{ 4 }; // array size row (const)
	const size_t M{ 4 }; // array size column (const)
	vector< vector<int> > arr(N, vector<int>(M));

	RandomArr(arr, N, M);
	cout << "The Random Array: " << endl;
	PrintArr(arr, N, M);
	cout << "Average: " << AverageArr(arr, N, M) << endl;
	cout << "Deviation: " << DeviationArr(arr, N, M) << endl;
	cout << "Determinant (Laplace's formula): " << DeterminantArr(arr, N, M) << endl;
	return 0;
}

void RandomArr(vector< vector<int> >& arr, const size_t& N, const size_t& M)
{
	for (size_t i{}; i < N; i++)
		for (size_t j{}; j < M; j++)
			arr[i][j] = rand() % 10;
}

void PrintArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M)
{
	for (size_t i{}; i < N; i++)
	{
		for (size_t j{}; j < M; j++)
			cout << std::setw(3) << arr[i][j] << "  ";
		cout << endl;
	}
}

double AverageArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M)
{
	int sum{};
	for (size_t i{}; i < N; i++)
		for (size_t j{}; j < M; j++)
			sum += arr[i][j];
	return sum / (N * M);
}

double DeviationArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M)
{
	double average{ AverageArr(arr, N, M) };
	int SumPrime{};
	for (size_t i{}; i < N; i++)
		for (size_t j{}; j < M; j++)
			SumPrime += arr[i][j] * arr[i][j];
	return sqrt(SumPrime / (N * M));
}

int DeterminantArr(const vector< vector<int> >& arr, const size_t& N, const size_t& M)
{
	if (!(N == 1 && M == 1))
	{
		int sum{};
		for (size_t j{}; j < M; j++)
		{
			/* A */
			vector< vector<int> > A(N - 1, vector<int>(M - 1));
			for (size_t k{}; k < N - 1; k++)
				for (size_t l{}; l < j; l++)
					A[k][l] = arr[k + 1][l];
			for (size_t k{}; k < N - 1; k++)
				for (size_t l{ j }; l < M - 1; l++)
					A[k][l] = arr[k + 1][l + 1];
			/* Sum */
			sum += pow(-1, j) * arr[0][j] * DeterminantArr(A, N - 1, M - 1);
		}
		return sum;
	}
	else
		return arr[0][0];
}