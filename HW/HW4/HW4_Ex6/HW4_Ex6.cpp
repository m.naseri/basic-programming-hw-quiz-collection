// MohammadMahdiNaseri_9423115
// HW4_Ex6

#include<iostream>
#include<iomanip>
#include<cmath>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	const double scale{ 20.0 };
	const double Pi{ asin(1) * 2 };
	for (int x{}; x < 2 * Pi * scale; x++)
	{
		int y{ static_cast<int>(scale * sin(x / scale)) };
		if (y >= 0)
		{
			cout << std::setw(static_cast<int>(1 + scale)) << std::setfill(' ');
			for (size_t i{}; i <= y; i++)
				cout << '*';
		}
		else
		{
			cout << std::setw(static_cast<int>(1 + scale + y)) << std::setfill(' ');
			for (size_t i{}; i <= -y; i++)
				cout << '*';
		}
		cout << endl;
	}

	return 0;
}