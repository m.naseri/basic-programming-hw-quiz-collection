#include<iostream>
#include<fstream>
#include<locale>
#include<vector>

using std::cout;
using std::cin;
using std::endl;

void permutation(std::string& str, std::vector<int> vect, std::ofstream& Output, size_t i0)
{
	if (i0 < str.length())
	{
		for (size_t i{}; i < str.length(); i++)
		{
			bool flag{ true };

			for (size_t j{}; j < vect.size(); j++)
			{
				if (i == vect[j])
				{
					flag = false;
					break;
				}
			}

			if (flag)
			{
				vect.push_back(i);
				cout << "i0: " << i0 << ", ch: " << str[i] << "; ";
				permutation(str, vect, Output, ++i0);
			}
		}
	}

	else if (i0 == str.length())
	{
		cout << endl;
	}
}

int main()
{
	std::ofstream Output{ "output.txt" };

	if (!Output)
	{
		std::cout << "input file does not exist" << std::endl;
		return 0;
	}

	std::string str;
	cout << "STR: ";
	cin >> str;
	std::vector<int> vect;
	
	permutation(str, vect, Output, 0);

	Output.close();
	return 0;
}