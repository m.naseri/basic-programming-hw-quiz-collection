#include<iostream>
#include<fstream>
#include<locale>
#include<vector>

using std::cout;
using std::cin;
using std::endl;

void permutation(std::string& str, std::ofstream& Output, size_t i0)
{
	if (i0 < 3)
	{
		for (size_t i{}; i < str.length(); i++)
		{
		// 	cout << "i0: " << i0 << ", ch: " << str[i] << "; ";
			if (i0 < 3)
			{
				cout << "i0: " << i0;
				permutation(str, Output, ++i0);
			}
		}
	}

	else if (i0 == str.length())
	{
		cout << endl;
		return;
	}
	return;
}

int main()
{
	std::ofstream Output{ "output.txt" };

	if (!Output)
	{
		std::cout << "input file does not exist" << std::endl;
		return 0;
	}

	std::string str;
	cout << "STR: ";
	cin >> str;
	std::vector<int> vect;

	permutation(str, Output, 0);

	Output.close();
	return 0;
}