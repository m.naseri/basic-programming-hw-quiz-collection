#include<iostream>

using std::cout;
using std::cin;
using std::endl;

void f1(int& a)
{
	a = 3;
}

int main()
{
	int a{ 2 };
	f1(a);
	cout << a << endl;
	return 0;
}