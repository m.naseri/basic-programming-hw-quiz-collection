#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int& f1(int& b)
{
	return b;
}

int main()
{
	int a{};
	f1(a) = 1;
	cout << a << endl;
	return 0;
}