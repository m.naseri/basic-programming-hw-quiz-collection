// MohammadMahdiNaseri_9423115
// HW5_Ex3

#include<iostream>
#include<fstream>
#include<locale>
#include<iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::string;

double** CrossProduct(double** R, double** P, size_t& M_R, size_t& N_R, size_t& N_P);

int main()
{
	std::ifstream MatricesFile{ "matrices.txt" };
	std::ifstream CheckFile{ "matrix-check.txt" };
	if (!(MatricesFile.is_open() && CheckFile.is_open()))
	{
		cout << "Error Opening the files..." << endl;
		return -1;
	}
	else
	{
		size_t M_R{}; // Result row size
		size_t N_R{}; // Result column size
		double** R{}; // Result
		string str0; // matrix informations

		for (size_t iter{}; MatricesFile >> str0; iter++) // reading
		{

			if (str0 == "MAT") // matrix available
			{
				/* P */
				size_t M_P{}; // matrix row size
				size_t N_P{}; // matrix column size
				MatricesFile >> M_P;
				MatricesFile >> N_P;
				double** P{ new double*[M_P] };
				for (size_t i{}; i < M_P; i++)
					P[i] = new double[N_P];
				for (size_t i{}; i < M_P; i++)
					for (size_t j{}; j < N_P; j++)
						MatricesFile >> P[i][j];

				/* Cross Product */
				if (iter == 0) // first matrix
				{
					M_R = M_P;
					N_R = N_P;
					R = new double*[M_R];
					for (size_t i{}; i < M_R; i++)
						R[i] = new double[N_R];
					for (size_t i{}; i < M_R; i++)
						for (size_t j{}; j < N_R; j++)
							R[i][j] = P[i][j];
				}

				else
				{

					/* T */
					size_t M_T{ M_R }; // Temp row size
					size_t N_T{ N_P }; // Temp column size
					double** T{ CrossProduct(R, P, M_R, N_R, N_P) }; // Temp

																	 /* New R */
					for (size_t i{}; i < M_R; i++)
						delete[] R[i];
					delete R;
					M_R = M_T; // matrix row size
					N_R = N_T; // matrix column size
					R = new double*[M_R]; // Result
					for (size_t i{}; i < M_R; i++)
						R[i] = new double[N_R];
					for (size_t i{}; i < M_R; i++)
						for (size_t j{}; j < N_R; j++)
							R[i][j] = T[i][j];

					/* T delete*/
					for (size_t i{}; i < M_T; i++)
						delete[] T[i];
					delete T;
				}

				/* P delete*/
				for (size_t i{}; i < M_P; i++)
					delete[] P[i];
				delete P;
			}
		}

		/* Result Check */
		CheckFile >> str0;
		bool ResultFlag{ true }; // false if the result is not right
		if (str0 == "MAT") // matrix available
		{
			size_t M_P{}; // matrix row size
			size_t N_P{}; // matrix column size
			CheckFile >> M_P;
			CheckFile >> N_P;
			if (!(M_R == M_P && N_R == N_P))
			{
				ResultFlag = false;
				return -1;
			}
			for (size_t i{}; i < M_R; i++)
			{
				for (size_t j{}; j < N_R; j++)
				{
					int temp{};
					CheckFile >> temp;
					if (abs(R[i][j] - temp) > 1E-3) // *double variables compare problem
					{
						ResultFlag = false;
						return -1;
					}
				}
			}
		}

		/* Result Print */
		if (ResultFlag)
		{
			cout << "MAT " << M_R << ' ' << N_R << endl;
			for (size_t i{}; i < M_R; i++)
			{
				for (size_t j{}; j < N_R; j++)
					cout << std::setw(2) << R[i][j] << ' ';
				cout << endl;
			}
		}

		/* R delete */
		for (size_t i{}; i < M_R; i++)
			delete[] R[i];
		delete R;
	}

	return 0;
}

double** CrossProduct(double** R, double** P, size_t& M_R, size_t& N_R, size_t& N_P)
{
	size_t M_P{ N_R };
	size_t M_T{ M_R }; // Temp row size
	size_t N_T{ N_P }; // Temp column size
	double** T{ new double*[M_T] }; // Temp
	for (size_t i{}; i < M_T; i++)
		T[i] = new double[N_T];
	for (size_t i{}; i < M_T; i++)
		for (size_t j{}; j < N_T; j++)
		{
			double sum{};
			for (size_t k{}; k < N_R; k++)
				sum += (R[i][k] * P[k][j]);
			T[i][j] = sum;
		}
	return T;
}