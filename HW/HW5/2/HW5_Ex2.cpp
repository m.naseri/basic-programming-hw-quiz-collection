// MohammadMahdiNaseri_9423115
// HW5_Ex2

#include<iostream>
#include<locale>
#include<fstream>
#include<vector>

using std::cout;
using std::cin;
using std::endl;

std::string ShiftString(std::string& str0, int diff);

int main()
{
	std::ifstream CodedFile{ "statement-coded.txt" };
	std::ofstream OutputFile{ "output.txt" };

	if (!(CodedFile.is_open() && OutputFile.is_open()))
	{
		cout << "Error Opening the files..." << endl;
		return -1;
	}
	else
	{
		std::vector<std::string> vect; // coded words

		for (std::string str0; CodedFile >> str0; vect.push_back(str0)); // reading

		for (int rand{ 1 }; rand < 26; rand++) // finding the key
		{
			std::vector<std::string> vectp; // uncoded words
			bool flag{ false }; // right loop flag (the key)

			for (std::string& str0 : vect) // shift
			{
				vectp.push_back(ShiftString(str0, rand));
				if (vectp.back() == "bolbol")
					flag = true;
			}

			if (flag == true) // cmd cout
			{
				cout << "Shift " << rand << ": ";
				for (std::string& str0 : vectp)
					cout << str0 << ' ';
				cout << endl;
			}

			OutputFile << "Shift " << rand << ": " << endl; // output file
			for (std::string& str0 : vectp)
				OutputFile << str0 << ' ';
			OutputFile << "\n\n";
		}
	}

	return 0;
}

std::string ShiftString(std::string& str0, int diff)
{
	std::string str1{};
	for (char ch : str0)
	{
		if (islower(ch))
		{
			if (ch - diff >= 'a')
				ch -= diff;
			else
				ch = 'z' + 1 - ('a' - (ch - diff));

		}
		else if (isupper(ch))
		{
			if (ch - diff >= 'A')
				ch -= diff;
			else
				ch = 'Z' + 1 - ('A' - (ch - diff));
		}
		else
			ch -= diff;
		str1 += ch;
	}
	return str1;
}