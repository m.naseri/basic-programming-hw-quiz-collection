// MohammadMahdiNaseri_9423115
// HW5_Ex4

#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<iomanip>
#include<conio.h>

using std::cout;
using std::cin;
using std::endl;

void RandomArr(int** table);
void PrintArray(int** table);
void moveUp(int** table, int OPosition[2]);
void moveLeft(int** table, int OPosition[2]);
void moveDown(int** table, int OPosition[2]);
void moveRight(int** table, int OPosition[2]);

int main()
{
	srand(static_cast<unsigned int>(time(NULL)));
	int** table{ new int*[4] };
	for (int i{}; i < 4; i++)
		table[i] = new int[4];

	int OPosition[2]{ 3, 3 }; // 0 Position

	RandomArr(table);
	PrintArray(table);

	char ch{};
	while (ch != 'q')
	{
		ch = _getch();
		switch (ch)
		{
		case 'w':
			moveUp(table, OPosition);
			break;
		case 'a':
			moveLeft(table, OPosition);
			break;
		case 's':
			moveDown(table, OPosition);
			break;
		case 'd':
			moveRight(table, OPosition);
			break;
		}
	}

	for (int i{}; i < 4; i++)
		delete[] table[i];
	delete table;
	return 0;
}

void RandomArr(int** table)
{
	for (size_t i = 0; i < 4; i++)
		for (size_t j = 0; j < 4; j++)
			table[i][j] = 0;

	for (size_t i = 0; i < 4; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			if (!(i == 3 && j == 3))
			{
				int temp{};
				bool flag{ false }; // Non-repetitive flag
				while (!flag) // Non-repetitive number creater
				{
					temp = rand() % 15 + 1;
					flag = true;
					for (size_t iprime{ 0 }; iprime < 4; iprime++)
					{
						for (size_t jprime{ 0 }; jprime < 4; jprime++)
						{
							if (table[iprime][jprime] == temp)
							{
								flag = false;
								break;
							}
						}
					}
				}
				table[i][j] = temp;
			}
		}
	}
	return;
}

void PrintArray(int** table)
{
	cout << "The Array: " << endl;
	for (size_t i = 0; i < 4; i++) // print
	{
		for (size_t j = 0; j < 4; j++)
			cout << std::setw(2) << table[i][j] << '\t';
		cout << endl;
	}
	cout << endl;
	return;
}

void moveUp(int** table, int OPosition[2])
{
	if (OPosition[0] - 1 >= 0)
	{
		int temp{};
		temp = table[OPosition[0]][OPosition[1]];
		table[OPosition[0]][OPosition[1]] = table[OPosition[0] - 1][OPosition[1]];
		table[OPosition[0] - 1][OPosition[1]] = temp;
		OPosition[0] -= 1;
		PrintArray(table);
	}
	return;
}

void moveLeft(int** table, int OPosition[2])
{
	if (OPosition[1] - 1 >= 0)
	{
		int temp{};
		temp = table[OPosition[0]][OPosition[1]];
		table[OPosition[0]][OPosition[1]] = table[OPosition[0]][OPosition[1] - 1];
		table[OPosition[0]][OPosition[1] - 1] = temp;
		OPosition[1] -= 1;
		PrintArray(table);
	}
	return;
}

void moveDown(int** table, int OPosition[2])
{
	if (OPosition[0] + 1 < 4)
	{
		int temp{};
		temp = table[OPosition[0]][OPosition[1]];
		table[OPosition[0]][OPosition[1]] = table[OPosition[0] + 1][OPosition[1]];
		table[OPosition[0] + 1][OPosition[1]] = temp;
		OPosition[0] += 1;
		PrintArray(table);
	}
	return;
}

void moveRight(int** table, int OPosition[2])
{
	if (OPosition[1] + 1 < 4)
	{
		int temp{};
		temp = table[OPosition[0]][OPosition[1]];
		table[OPosition[0]][OPosition[1]] = table[OPosition[0]][OPosition[1] + 1];
		table[OPosition[0]][OPosition[1] + 1] = temp;
		OPosition[1] += 1;
		PrintArray(table);
	}
	return;
}