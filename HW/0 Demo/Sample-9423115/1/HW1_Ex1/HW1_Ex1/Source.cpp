// MohammadMahdiNaseri_9423115
// Homework1_Exercise1

#include <iostream>
#include <conio.h>

int main()
{
	int std_num{ 9423115 }; // Student Number
	std::cout
		<< "-----------------------------------\n"
		<< "This is my \"first\" program in C++\n"
		<< "***********************************\n"
		<< "I love C++ coding\n"
		<< "***********************************\n"
		<< "I am going to work very \'hard\'\n"
		<< "+++++++++++++++++++++++++++++++++++\n"
		<< "\\\\\\\\ MohammadMahdi Naseri: " << std_num << " \\\\\\\\\n" // one "\\" for each "\"
		<< "***********************************\n";
	_getch();
	return 0;
}