//#include <iostream>
//#include <ctime>
//#include <cstdlib>
//
//using namespace std;
//
//int main() 
//{
//	cout << "\n\n********* code #1 *********\n\n";
//	srand(time(NULL));
//
//	int rand1[16]{};
//
//	for (int i = 0; i < 15; i++)
//	{
//		rand1[i] = rand() % 15;
//		cout << rand1[i] << endl;
//	}
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main() {
//
//	cout << "\n\n********* code #2 *********\n\n";
//
//	int array[20]{};
//
//	for (int i = 0; i < 20; i++)
//	{
//		array[i] = i;
//		cout << array[i] << endl;
//	}
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main() {
//
//	cout << "\n\n********* code #4 *********\n\n";
//
//	int a{ 5 }, b{};
//	do
//	{
//		cout << a-- << endl;
//		cout << b++ << endl;
//	} while (a > 2);
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main() {
//
//	cout << "\n\n********* code #5 *********\n\n";
//
//	char x = 'Y';
//	while (x == 'Y')
//	{
//		//...
//		cout << "Continue? (Y/N)";
//		cin >> x;
//	}
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main() {
//
//	cout << "\n\n********* code #6 *********\n\n";
//
//	double x = 1.0 / 2.0;
//	cout << pow(4, x) << "  " << pow(9, x) << "  " << pow(16, x) << endl;
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main() {
//
//	cout << "\n\n********* code #7 *********\n\n";
//
//	int num;
//	cin >> num;
//	switch (num)
//	{
//	case (1) :
//		cout << "num is equal to 1" << endl;
//		break;
//	case (2) :
//		cout << "num is equal to 2" << endl;
//		break;
//	case (3) :
//		cout << "num is equal to 3" << endl;
//		break;
//	}
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main() {
//
//	cout << "\n\n********* code #3 *********\n\n";
//
//	int array2[10][10];
//	for (int i{ 0 }; i < 10; i++)
//	{
//		for (int k{ 0 }; k < 10; k++)
//		{
//			array2[i][k] = i*k;
//			cout << array2[i][k] << " ";
//		}
//			cout << endl;
//	}
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//double multiply(int x, int y)
//{
//	return x * y;
//}
//
//int main() {
//
//	cout << "\n\n********* code #8 *********\n\n";
//
//	int a{ 2 }, b{ 3 };
//	cout << multiply(a, b) << endl;
//
//	return 0;
//}