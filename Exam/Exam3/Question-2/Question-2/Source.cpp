#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	int N{}, M{}; // row & column of the matrix
	cout << "N: ";
	cin >> N;
	cout << "M: ";
	cin >> M;
	if (M > 10 || N > 10)
	{
		cout << "Error..." << endl;
		return 0;
	}
	cout << endl;

	// cin: 
	int matrix[10][10]{};
	for (int i{}; i < N; i++)
	{
		for (int j{}; j < M; j++)
		{
			cout << "Number " << i + 1 << ", " << j + 1 << ": ";
			cin >> matrix[i][j];
		}
		cout << endl;
	}

	// print:
	for (int i{}; i < N; i++)
	{
		if (i % 2 == 0)
			for (int j{}; j < M; j++)
			{
				cout << matrix[i][j] << " ";
			}
		else
			for (int j{ M - 1}; j >= 0; j--)
			{
				cout << matrix[i][j] << " ";
			}
		cout << endl;
	}

	cout << endl;
	return 0;
}