#include<iostream>
#include<cmath>
#include<vector>

using std::cout;
using std::cin;
using std::endl;

double f1(unsigned int N, int* factor, double x);
double f2(double x);

int main()
{
	//const unsigned int N{ 2 }; // Max degree or array size + 1
	//int factor[N + 1]{}; // factors

	//// factors  cin
	//for (size_t i{ 0 }; i <= N; i++)
	//{
	//	cout << "X" << " ^ " << i << " * ";
	//	unsigned int temp{};
	//	cin >> temp;
	//	factor[i] = temp;
	//}

	double X1{};
	cout << "X1: ";
	cin >> X1;
	double X2{};
	cout << "X2: ";
	cin >> X2;

	// x0 finding
	double eps{ 1 };

	while (abs(eps) > 1E-5)
	{
		double XO{ (X1 + X2) / 2 };
		if (f2(XO)*f2(X2) < 0)
			X1 = XO;
		if (f2(XO)*f2(X1) < 0)
			X2 = XO;
		eps = abs(f2(X1)) < abs(f2(X2)) ? f2(X1) : f2(X2);
	}

	cout << "X = " << (abs(f2(X1)) < abs(f2(X2)) ? X1 : X2) << endl;
	cout << "f(X) = " << (abs(f2(X1)) < abs(f2(X2)) ? f2(X1) : f2(X2));
	cout << endl;
	return 0;
}

//double f1(unsigned int N, int* factor, double x)
//{
//	double f1{};
//	for (size_t i{ 0 }; i <= N; i++)
//		f1 += pow(x, i) * factor[i];
//	return f1;
//}

double f2(double x)
{
	return x*cos(x) - log10(x);
}

