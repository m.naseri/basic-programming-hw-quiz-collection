#include<iostream>

using namespace std;

int factorial(int num)
{
	int product{ 1 };
	for (int i{ 1 }; i <= num; i++)
		product *= i;
	return product;
}

int main()
{
	int num{};
	cout << "N: ";
	cin >> num;
	cout << num << "! = " << factorial(num) << endl;
	return 0;
}