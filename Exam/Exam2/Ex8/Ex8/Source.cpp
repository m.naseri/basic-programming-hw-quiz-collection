#include<iostream>
#include<locale>

using namespace std;

int main()
{
	char character[11]{};
	unsigned int VowelCounter{}, ConsonantCounter{}, DigitCounter{}, OtherCounter{}; // counters
	cin.getline(character, 11);

	for (int i{}; i < 10; i++)
	{
		char ch{ character[i] };
		if (isalpha(ch))
			ch = tolower(ch);
		if (ch == 'a' || ch == 'e' || ch == 'o' || ch == 'i' || ch == 'u')
			ConsonantCounter++;
		else if (isalpha(ch))
			VowelCounter++;
		else if (isdigit(ch))
			DigitCounter++;
		else
			OtherCounter++;
	}

	cout << "Number of Consonant characters : " << ConsonantCounter << endl
		<< "Number of Vowel characters:" << VowelCounter << endl
		<< "Number of Digit characters: " << DigitCounter << endl
		<< "Number of other characters: " << OtherCounter << endl;

	return 0;
} 