#include<iostream>
#include<iomanip>

using namespace std;

int factorial(int num)
{
	int product{ 1 };
	for (int i{ 1 }; i <= num; i++)
		product *= i;
	return product;
}

int main()
{
	int n{};
	cout << "N: ";
	cin >> n;
	for (int i{}; i <= n; i++)
	{
		for (int k{}; k < n - i; k++) // space print (real condition: 2 * k < (2 * (n + 1) - 1) - (2 * (i + 1) - 1))
			cout << " ";
		for (int j{}; j <= i; j++)
			cout << setw(3) << factorial(i) / (factorial(i - j) * factorial(j)) << " ";
		cout << endl;
	}
	return 0;
}