// MohammadMahdiNaseri_9423115
// Exam2_Example5

#include <iostream>

using namespace std;

int main()
{
	int num[8]{ 1, 12, 15, 7, 4, 53, 2, 32 };
	for (int i : num)
		cout << i << " x " << i << " = " << i * i << endl;
	return 0;
}