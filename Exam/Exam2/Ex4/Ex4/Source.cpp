// MohammadMahdiNaseri_9423115
// Exam2_Example4

#include <iostream>

using namespace std;

int main()
{
	int number[10]{}, max{}, sum{};
	for (int i = 0; i < 10; i++)
	{
		cout << "Number " << i + 1 << ": ";
		cin >> number[i];
		max = number[i] > max ? number[i] : max;
		sum += number[i];
	}
	cout << "Max: " << max << endl
		<< "Average: " << sum / 10 << endl;
	return 0;
}