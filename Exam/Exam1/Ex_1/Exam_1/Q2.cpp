#include<iostream>
#include<iomanip>
using std::cout;
using std::cin;
using std::endl;

int main()
{
	cout << std::setw(3) << "*" << "\t";
	for (int i = 0; i <= 10; i++)
		for (int j = 0; (j <= 10) && !(i == 0 && j == 0); j++)
		{
				cout << std::setw(3) << i * j << "\t";
		}
	return 0;
}