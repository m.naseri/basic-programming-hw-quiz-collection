#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	int x{}, y{}, z{}; // 3 variables for inputting
	cout << "x, y, z: ";
	cin >> x >> y >> z;
	switch (z)
	{
		case 0:
		{
			cout << "x + y = " << x + y << endl;
			break;
		}
		case 1:
		{
			cout << "x - y = " << x - y << endl;
			break;
		}
		case 2:
		{
			cout << "x * y = " << x * y << endl;
			break;
		}
		default:
			cout << "Error: Please Enter z = 0 or 1 or 3" << endl;
	}
	return 0;
}