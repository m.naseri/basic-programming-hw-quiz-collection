#include<iostream>
#include<iomanip>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	int N{}; // Number of rows
	cout << "N: ";
	cin >> N;
	for (int i = 1; i <= N; i++) // row changer
	{
		for (int j = 1; j <= N - i; j++) // space printer ( j <= (1/2) * ((2N-1) - (2i-1)) )
			cout << ' ';
		for (int j = 1; j <= i; j++) // number printer
			cout << j << ' ';
		cout << endl; // next row
	}
	cout << endl;
	return 0;
}