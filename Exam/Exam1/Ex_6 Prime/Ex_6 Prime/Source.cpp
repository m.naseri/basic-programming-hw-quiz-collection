#include<iostream>
#include<cmath>
#include<conio.h>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	int Num{}; // the Number for checking (i * j)
	int iNum{}; // inversed Num
	int DCount{}; // digits counter of i * j
	int Max{}; // maximum of Num s!
	int temp{}; // just temporary
	for (int i = 999; i >= 100; i--)
		for (int j = 999; j >= i; j--)
		{
			Num = i * j;
			temp = Num;
			for (DCount = 0; temp != 0; DCount++) // digits Number counting
				temp /= 10;
			temp = Num;
			for (int k = 0; k < DCount; k++) // digits sepration & inversing
			{
				iNum += (temp % 10) * static_cast<int>(pow(10, DCount - (k + 1)));
				temp /= 10;
			}
			if (iNum == Num) // comparing
				Max = (Max > iNum) ? Max : iNum;
			iNum = 0;
		}
	cout << "The Number is: " << Max << endl;
	_getch();
	return 0;
}