#include<iostream>
#include<cmath>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	long double pi{}, pr{1}; // PI number, precision
	unsigned long long int i{}; // public counter
	for (i = 1; pr > 1E-6; i++)
	{
		pi += 4 * pow(-1, i + 1) / (2 * i - 1);
		pr = abs(pi - acos(-1));
		// cout << pi << "\t";
	}
	cout << endl << "Pi: " << pi << endl;
	cout << "Number of loops: " << i - 1 << endl;
	return 0;
}