#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	int x{}; // the 7 digits number
	int xDigits[7]{}; // the digits: x = x6x5x4x3x2x1x0
	cout << "Enter a 7 digits number: ";
	cin >> x;
	int temp{ x }; // just temporary
	if (x >= 1000000 && x < 10000000) // check the digits number
	{
		cout << "Without 0: ";
		for (int i = 0; i <= 6; i++) // digits sepration
		{
			xDigits[i] = temp % 10;
			temp /= 10;
		}
		for (int i = 6; i >= 0; i--) // digits print
		{
			if (xDigits[i] != 0)
				cout << xDigits[i];
		}
		cout << endl;
	}
	else
		cout << "Error! A 7 digits please!" << endl;
	return 0;
}