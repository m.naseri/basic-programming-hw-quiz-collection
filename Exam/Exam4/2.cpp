#include<iostream>
#include<string>
#include<vector>
#include<locale>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

void Replace(vector<string>& vect, size_t& i0, size_t& i1)
{
	string StrTemp{ vect[i0] };
	vect[i0] = vect[i1];
	vect[i1] = StrTemp;

	return;
}

void Sort(vector<string>& vect, size_t& i0)
{
	for (size_t i1 = i0; i1 < vect.size(); i1++)
	{
		if (vect[i1] < vect[i0])
			Replace(vect, i0, i1);
	}

	return;
}

int main()
{
	vector<string> vect;

	/* Input */
	string StrTemp{ 'y' };
	while (StrTemp == "y")
	{
		cout << "EnterName: ";
		cin >> StrTemp;
		vect.push_back(StrTemp);
		cout << "Any String Else? (y/n) ";
		cin >> StrTemp;
		if (StrTemp == "n")
			break;
	}

	/* Sort */
	for (size_t i = 0; i < vect.size(); i++)
	{
		Sort(vect, i);
	}

	/* Print */
	cout << endl << "SortedItems: " << endl;
	for (auto& i : vect)
	{
		cout << i << endl;
	}

	return 0;
}
