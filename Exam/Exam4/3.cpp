#include<iostream>
#include<string>
#include<vector>
#include <fstream>

using namespace std;

int main() {
	ofstream myfile("output.txt");
	if (myfile.is_open())
	{
		vector<int> vect;
		vector<int> vectprime;
		size_t M{};
		cout << "M: ";
		cin >> M;
		size_t N{};

		/* Prime */
		while (vectprime.size() < M)
		{
			vect.clear();
			vectprime.clear();
			N += 500;

			/* Values */
			for (size_t i = 0; i <= N; i++)
			{
				vect.push_back(i);
			}

			/*PrimeDetection*/
			for (size_t i = 2; i <= N; i++)
			{
				if (vect[i] != 0)
					for (size_t j = i + 1; j <= N; j++)
					{
						if (vect[j] % vect[i] == 0)
							vect[j] = 0;
					}
			}

			/* VectPrime */
			for (auto& i : vect)
			{
				if (i)
					vectprime.push_back(i);
			}
		}

		/* Print */
		for (auto& i : vectprime)
		{
			static int j{ 1 };
			if (j > M)
				break;
			if (i)
			{
				cout << j << ". " << i << endl;
				myfile << j << ". " << i << endl;
			}
			j += 1;
		}

		myfile.close();
	}
	else cout << "Unable to open file";
	return 0;
}