﻿#include<iostream>
#include<cmath>

using namespace std;

long double aFunction(const long double&);
long double bFunction(const long double&);

int main() 
{
	cout.precision(20); // show 20 digits after point

	pf = aFunction; // Pointer initialized

	// ∫a b f(x)
	CIntegral f1(pf, a, b);
	f1.exec(); // Calculate the integral

	// return the results
	cout << "Result:" << f1.getResult() << endl;

	pf = bFunction; // Now it points to bFunction

	CIntegral f2(pf, a, b);
	f2.exec();
	cout << f2.getResult() << endl;

	return 0;
}

// these can be any arbitrary functions
long double aFunction(const long double& x){
	return x * cos(x);
}

long double bFunction(const long double& x){
	return 1 / x;
}