#include <iostream>
#include <iomanip>

int main()
{
	size_t swv{ 10 }; // set width value
	std::cout << std::setw(swv) << "Character" << std::setw(swv)
		<< "Base 10" << std::setw(swv) << "Base 16" << std::endl;

	for (char ch{ 'a' }; ch <= 'z'; ch++)
	{
		std::cout << std::setw(swv) << ch
			<< std::setw(swv) << static_cast<int>(ch)
			<< std::setw(swv) << std::setbase(16) << static_cast<int>(ch);
		std::cout << std::endl;
	}
	for (char ch{ 'A' }; ch <= 'Z'; ch++)
	{
		std::cout << std::setw(swv) << ch
			<< std::setw(swv) << static_cast<int>(ch)
			<< std::setw(swv) << std::setbase(16) << static_cast<int>(ch);
		std::cout << std::endl;
	}

	return 0;
}