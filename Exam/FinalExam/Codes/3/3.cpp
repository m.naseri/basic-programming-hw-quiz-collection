#include <iostream>
#include <string>

int main()
{
	std::string str;
	std::cout << "str: ";
	std::cin >> str;

	std::string MaxSubStr;
	size_t MaxSubStrRepeat{};
	for (size_t SubStrLength{ 2 }; SubStrLength <= str.size(); SubStrLength++) // subSTR length
		for (size_t SubStrFirst{}; SubStrFirst <= str.size() - SubStrLength; SubStrFirst++) // subSTR Maker
		{
			std::string SubStr{ str, SubStrFirst, SubStrLength };
			size_t SubStrRepeat{};
			for (size_t j{}; j <= str.size() - SubStrLength; j++) // subSTR Checker
			{
				std::string temp{ str, j, SubStrLength };
				std::cout << SubStr << " & " << temp << ": ";
				if (temp == SubStr)
					SubStrRepeat += 1;
			}

			if (SubStrRepeat >= MaxSubStrRepeat)
			{
				MaxSubStrRepeat = SubStrRepeat;
				MaxSubStr = SubStr;
			}
		}

	std::cout << "MaxSubStr: " << MaxSubStr << std::endl
		<< "MaxSubStrRepeat: " << MaxSubStrRepeat << std::endl;

	return 0;
}
