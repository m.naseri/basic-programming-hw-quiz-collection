#include <iostream>
#include <vector>
#include <string>

int SearchVect(const std::vector<size_t>& v, size_t num, size_t start, size_t end)
{
	size_t mid{ (start + end) / 2 };
	if (v[mid] == num)
		return mid;
	if (start >= end)
		return -1;
	if (num < v[mid])
		return SearchVect(v, num, start, mid - 1);
	else
		return SearchVect(v, num, mid + 1, end);
}

void PrimesFind(std::vector<size_t>& vectprime)
{
	std::vector<size_t> vect;
	size_t N{ 9999 };

	/* Values */
	for (size_t i{}; i <= N; i++)
		vect.push_back(i);

	/*PrimeDetection*/
	for (size_t i{ 2 }; i <= static_cast<size_t>(sqrt(N)); i++)
		if (vect[i] != 0)
			for (size_t j{ i + 1 }; j <= N; j++)
				if (vect[j] != 0)
					if (vect[j] % vect[i] == 0)
						vect[j] = 0;

	/* VectPrime */
	for (auto& i : vect)
		if (i > 999)
			vectprime.push_back(i);
}

bool isPermuted(std::string str1, std::string str2)
{
	for (size_t i{}; i < str1.size(); i++) // on the str1 arguments
	{
		if (str2.find_first_of(str1[i]) > 1000) // if(could not find)
		{
			return false;
		}
		else
			str2[str2.find_first_of(str1[i])] = 'T';
	}

	return true;
}

int main()
{
	std::vector<size_t> vect;
	PrimesFind(vect);

	std::vector<std::string> strvect;
	strvect.resize(3);
	std::vector<size_t> vectprime;
	for (size_t i{}; i < vect.size(); i++) // first number
	{
		strvect[0] = std::to_string(vect[i]);
		for (size_t j{ i + 1 }; j < vect.size(); j++) // second number
		{
			strvect[1] = std::to_string(vect[j]);
			if (isPermuted(strvect[0], strvect[1])) // first and second are permuted
			{
				size_t DestinationValue{ 2 * vect[j] - vect[i] };
				if (DestinationValue < 10000)
				{
					int index{ SearchVect(vect, DestinationValue, j + 1, vect.size() - 1) }; // third number
					if (index != -1) // third number is prime too
					{
						strvect[2] = std::to_string(DestinationValue);
						if (isPermuted(strvect[1], strvect[2]))
						{
							for (auto& i : strvect)
								std::cout << i << " ";
							std::cout << std::endl;
						}
					}
				}
			}
		}
	}

	return 0;
}