#pragma once
class Complex
{
public:
	Complex();
	Complex(double a, double b);
	Complex add(const Complex& c);
	Complex mul(const Complex& c);
	~Complex();

	double real, img; // real and imaginary part
};

