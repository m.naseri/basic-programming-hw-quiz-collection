#include <iostream>
#include <string>
#include <fstream>
#include "complex.h"

int main()
{
	/* FileReading */
	std::ifstream ComplexFile{ "complex.txt" };
	if (!ComplexFile.is_open())
	{
		std::cout << "Error openning the file..." << std::endl;
		return -1;
	}
	const size_t N{ 100 }; // ComplexNumber.size()
	Complex ComplexNumber[N];
	std::string line;
	for (size_t ComplexNumberIndex{}; ComplexFile >> line; ComplexNumberIndex++)
	{
		std::string RealPart, ImgPart;
		for (size_t i{ 1 }; line[i] != '+'; i++)
			RealPart += line[i];
		for (size_t i{ RealPart.size() + 2 }; line[i] != 'j'; i++) // + 2 : ) & +
			ImgPart += line[i];

		ComplexNumber[ComplexNumberIndex].real = std::stod(RealPart);
		ComplexNumber[ComplexNumberIndex].img = std::stod(ImgPart);
	}

	/* Add & Mul*/
	Complex ComplexSum{ .0, .0 };
	Complex ComplexProduct{ 1., .0 };
	for (auto& C : ComplexNumber)
	{
		ComplexSum = ComplexSum.add(C);
		ComplexProduct = ComplexProduct.mul(C);
	}

	/* Print */
	std::cout << "ComplexSum: (" << ComplexSum.real << ", "
		<< ComplexSum.img << ")" << std::endl
		<< "ComplexProduct: (" << ComplexProduct.real << ", "
		<< ComplexProduct.img << ")" << std::endl;

	return 0;
}
