#include "complex.h"

Complex::Complex()
{
}

Complex::Complex(double a, double b) : real{ a }, img{ b }
{
}

Complex Complex::add(const Complex& c)
{
	return Complex(real + c.real, img + c.img);
}

Complex Complex::mul(const Complex& c)
{
	return Complex(real*c.real - img*c.img, real*c.img + img*c.real);
}


Complex::~Complex()
{
}
