#include <iostream>
#include <string>

bool isConcurrent(std::string& str);

int main()
{
	int theNum{};
	int i0{}, j0{}; // theNum = i0 * j0;
	for (size_t i{ 999 }; i > 99; i--)
		for (size_t j{ i }; j > 99; j--)
		{
			unsigned int temp{ i*j };
			if (temp > theNum)
			{
				std::string str{ std::to_string(temp) };
				if (isConcurrent(str))
				{
					theNum = (temp);
					i0 = i;
					j0 = j;
				}
			}
			else
				break;
		}

	std::cout << i0 << " * " << j0 << " = " << theNum << std::endl;

	return 0;
}

bool isConcurrent(std::string& str)
{
	size_t N{ str.size() };
	for (size_t i{}; i <= (N - 1) / 2; i++)
		if (str[i] != str[N - 1 - i])
			return false;

	return true;
}